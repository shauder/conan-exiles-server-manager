Function NeedsUpdate() {
    $SteamCMD = & $SteamCMDPath\SteamCMD.exe +login anonymous +app_info_update 1 +app_info_print 443030 +app_info_print 443030 +quit | Select-String -Context 0,5 "branches" | foreach-object {$_.Context.Postcontext} | Select-String "BuildID"
    $A,$B,$C,$D = $SteamCMD -split """		"""
    $BuildID = $B -replace '"',''
    if ( $CurrentVersion -eq $BuildID )
    {
        return $false
    }
    else
    {
        $BuildID | Out-File "$GamePath\currentversion.txt"
        return $true
    }
}

Function BackUp() {
    Copy-Item "$GamePath\ConanSandbox\Saved\game.db" "$GamePath\ConanSandbox\Saved\game-$Date-updated-server.db"
}

Function UpdateServer() {
    & $SteamCMDPath\SteamCMD.exe +login anonymous +force_install_dir "$GamePath" +app_update 443030 validate +quit
}

Function RunServer() {
    & "$GamePath\ConanSandbox\Binaries\Win64\ConanSandboxServer-Win64-Test.exe" "ConanSandbox?Multihome=192.168.0.3?GameServerPort=27015?GameServerQueryPort=27016?MaxPlayers=70?listen?AdminPassword=CHANGEME" -nosteamclient -game -server -log -ServerName="Dudes of Gaming"
}

# main()
$Date = Get-Date -format "dd-MMM-yyyy-HH-mm"
$GamePath = "C:\Exiles"
$SteamCMDPath = "C:\steamcmd"
$ConanProcess = Get-Process ConanSandboxServer-Win64-Test -ErrorAction SilentlyContinue
$CurrentVersion = Get-Content "$GamePath\currentversion.txt"
Remove-Item "$SteamCMDPath\appcache\*.vdf"

if ( NeedsUpdate )
{
    if ( $ConanProcess ) { Stop-Process -Name "ConanSandboxServer-Win64-Test" }
    BackUp
    UpdateServer
    RunServer
    exit
}
else
{
    if ( -Not $ConanProcess ) { RunServer }
    exit
}
